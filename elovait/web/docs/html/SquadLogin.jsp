<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Sadnick Studios" />
    <title>HeartLand Gamers</title>

    <link href="/docs/css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!-- including top meuu -->
<div id="centeredmenu">
    <ul>
        <li><a href="../../index.jsp">Home</a></li>

        <li><a href="#" class="active">Squad Members</a>
            <ul>
                <li><a href="User?id=Nagol">Nagol</a></li>
                <li><a href="User?id=Dsadnick">Dsadnick</a></li>
                <li><a href="User?id=Yamaha YZF">Yamaha YZF</a></li>
                <li><a href="User?id=SleepyHatch">Sleepy Hatch</a></li>
            </ul>
        </li>

        <li>
            <a href="#">About Squad</a>
        </li>

        <li><a href="#">Contact/Register</a>
            <ul >
                <li><a href="#">Register</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </li>

        <li><a href="#">Devlopment Stuff</a>
            <ul class="last">
                <li><a href="HomePage" >HOME PAGE JAVA CLASS</a></li>
                <li><a href="PageTwo" >Test</a></li>
                <li><a href="docs/html/Login.jsp" >Login Test</a></li>
                <li><a href="JstlTest" >Test for JSTL</a></li>
                <li><a href="User" >UserGetTest</a></li>
            </ul>
        </li>

        <li><a href="SquadLogin.jsp">SQUAD LOGIN</a></li>

    </ul>
</div>

<div class="containerDiv">

    <div class="leftDiv"></div>


    <div class="mainDiv">
        <div class="login">
            <div class="heading">

                <c:if test="${logedInError=='true'}">
                    <h2 style="color:red;">UserName/Password Combination incorrect</h2>
                </c:if>
                <h2>Sign in</h2>
                <form action="/SquadLoginServlet">

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" name="userName" class="form-control" placeholder="Username or email">
                    </div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>

                    <button type="submit" class="float">Login</button>
                </form>
            </div>
        </div>
    </div>


    <div class="rightDiv"></div>

</div>

<div class="footer">TEST TEXT</div>

</body>


</html>