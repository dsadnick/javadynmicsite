<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: dsadnick
  Date: 6/23/15
  Time: 3:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

  <table border="1">
    <c:forEach var="a" items="${data}">
      <tr>
        <td>${a}</td>
      </tr>
    </c:forEach>
  </table>

  <c:forEach begin="0" end="255" var="i">
    <span style='color:rgb(
         <c:out value="${i}"/>,
         <c:out value="${i}"/>,
         <c:out value="${i}"/>);'>
      <c:out value="${i}"/></span> <br />
  </c:forEach>

</body>
</html>
