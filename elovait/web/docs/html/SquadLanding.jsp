<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Sadnick Studios" />
    <title>HeartLand Gamers</title>

    <link href="/docs/css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!-- including top meuu -->
<div id="centeredmenu">
    <jsp:include page="../IncludeTemplates/topMenu.jsp" />
</div>

<div class="containerDiv">

    <div class="leftDiv"></div>


    <div class="mainDiv">
    <c:if test="${logedIn=='true'}">
        <article>
            <form action="/UploadServlet" 	method="post" enctype="multipart/form-data">

                <h3>Hello, ${userName}</h3>
                <br />
                <input type="checkbox" name="teamWin">Team Win?<br>
                <br />
                <label>Upload Winning picture:</label>
                <input type="file" name="file"/>
                <br />
                <label for="message">Comment:</label>
                <textarea cols="50" rows="5" id="message" name="comment"></textarea>
                <br />
                <p>
                    <button type="submit" >Submit Win.</button>
                </p>
            </form>
        </article>

    </c:if>

     <c:if test="${empty logedIn}">
            <p>User Is Not Loged In. Redirect to login.jsp.</p>
      </c:if>






    </div>


    <div class="rightDiv"></div>

</div>

<div class="footer">TEST TEXT</div>

</body>


</html>

