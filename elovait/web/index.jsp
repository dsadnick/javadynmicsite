<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="author" content="Sadnick Studios" />
  <title>HeartLand Gamers</title>

  <link href="docs/css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!-- including top meuu -->
<div id="centeredmenu">
    <ul>
        <li><a href="index.jsp">Home</a></li>

        <li><a href="#" class="active">Squad Members</a>
            <ul>
                <li><a href="User?id=Nagol">Nagol</a></li>
                <li><a href="User?id=Dsadnick">Dsadnick</a></li>
                <li><a href="User?id=Yamaha YZF">Yamaha YZF</a></li>
                <li><a href="User?id=SleepyHatch">Sleepy Hatch</a></li>
            </ul>
        </li>

        <li>
            <a href="#">About Squad</a>
        </li>

        <li><a href="#">Contact/Register</a>
            <ul >
                <li><a href="#">Register</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </li>

        <li><a href="#">Devlopment Stuff</a>
            <ul class="last">
                <li><a href="HomePage" >HOME PAGE JAVA CLASS</a></li>
                <li><a href="PageTwo" >Test</a></li>
                <li><a href="docs/html/Login.jsp" >Login Test</a></li>
                <li><a href="JstlTest" >Test for JSTL</a></li>
                <li><a href="User" >UserGetTest</a></li>
            </ul>
        </li>

        <li><a href="docs/html/SquadLogin.jsp">SQUAD LOGIN</a></li>

    </ul>
</div>

<div class="containerDiv">

    <div class="leftDiv"></div>


    <div class="mainDiv">
        <br />
        <br />
        <br />
        <br />
    </div>


    <div class="rightDiv"></div>

</div>

<div class="footer">TEST TEXT</div>

</body>


</html>

