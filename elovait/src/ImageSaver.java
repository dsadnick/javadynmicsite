import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Date;

/**
 * Created by dsadnick on 7/24/15.
 */
public class ImageSaver {

    public void saveImage(InputStream fileContent, String fileName){


        File uploads = new File("images/");

        File file = new File(uploads,fileName);


        try{
            InputStream input = fileContent;
            Files.copy(input, file.toPath());
        } catch (Exception e){
            e.printStackTrace();
        }

    }

}
