
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by dsadnick on 6/16/15.
 */
public class MySQLAccess{


    private static final String dbClassName = "com.mysql.jdbc.Driver";
    private static final String CONNECTION =
            "jdbc:mysql://127.0.0.1/mydb";
    java.sql.Connection connection = null;

    public ArrayList readDataBase() throws Exception {

        ResultSet resultSet = null;
        ArrayList<InfoBean> infoBean = new ArrayList<>();

    try {

            Class.forName(dbClassName).newInstance();

            connection = DriverManager.getConnection(CONNECTION, "root", "root");
            Statement statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM mydb.users");
            //connection.close();

            while(resultSet.next()){
                InfoBean tempBean = new InfoBean();

                String userName = resultSet.getString(1);
                String pw = resultSet.getString(2);
                String ip = resultSet.getString(3);

                tempBean.setIp(ip);
                tempBean.setUserName(userName);
                tempBean.setUserPassword(pw);

                infoBean.add(tempBean);

            }
        } catch (Exception e){
            System.out.println("Broken at catch statment MySQLAccess.java");
            e.printStackTrace();
        } finally {
            try{
                connection.close();
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Broken in closing connection");

            }
        }

        return infoBean;
    }

}
