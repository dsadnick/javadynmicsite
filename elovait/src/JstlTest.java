import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet("/JstlTest")
/**
 * Created by dsadnick on 6/23/15.
 */
public class JstlTest extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, HTTPException, IOException {

        ArrayList<String> tempList = new ArrayList<>();

        tempList.add("one");
        tempList.add("two");
        tempList.add("three");
        tempList.add("four");
        tempList.add("five");

        request.setAttribute("data", tempList);

        request.getRequestDispatcher("docs/html/JstlTest.jsp").forward(request, response);



    }

}
