import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by dsadnick on 6/13/15.
 */

@WebServlet("/PageTwo")
public class PageTwo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {


        PrintWriter out = res.getWriter();

        out.println("<h1>Dylans test is for kims love</h1>");
        out.flush();
        out.close();

    }
}
