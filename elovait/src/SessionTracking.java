import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

@WebServlet("/SessionTracking")
/**
 * Created by dsadnick on 6/15/15.
 */
public class SessionTracking extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String userName = (String)session.getAttribute("userName");
        Date dateLast = (Date)session.getAttribute("lastLogin");

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("Username: " + userName);
        printWriter.println("LastLogin: " + dateLast);
        printWriter.println("TIME NOW : " + new Date());


    }
}
