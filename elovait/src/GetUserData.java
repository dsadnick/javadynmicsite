import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/User")
/**
 * Created by dsadnick on 7/2/15.
 */
public class GetUserData extends HttpServlet  {


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String testName = request.getParameter("id");



        request.setAttribute("userName", testName);

        request.getRequestDispatcher("docs/html/UserDetails.jsp").forward(request, response);
    }

}
