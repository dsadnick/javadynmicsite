import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;

@WebServlet("/HomePage")
/**
 * Created by dsadnick on 6/11/15.
 */

public class HomePage extends HttpServlet{



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long a = System.currentTimeMillis();
        PrintWriter out = resp.getWriter();
        out.println("<b>Dylan Sadnick is super cool</b>");

        MySQLAccess mySQLAccess = new MySQLAccess();


        try {
            ArrayList<InfoBean> infoBean = mySQLAccess.readDataBase();

            String userName = req.getParameter("userName");

            out.println("<h1>Hello " + userName + "</h1>");

            if (infoBean == null){
            } else {
                out.println("Size is : " + infoBean.size() + "<br />");
                out.println("<table border='1'>" +
                        "  <tr>" +
                        "    <th>USERNAME</th>" +
                        "    <th>Password </th>" +
                        "    <th>IP</th>" +
                        "  </tr>");
                for (int i = 0; i < infoBean.size(); i++) {
                    out.println("<tr> <td>" + infoBean.get(i).getUserName() +
                            " </td><td>" + infoBean.get(i).getUserPassword() +
                            "</td><td>" + infoBean.get(i).getIp() + "</td></tr>");
                }
                out.println("</table>");

            }
            long b = System.currentTimeMillis();
            long elapsedTime = b - a;
            out.println(infoBean.size() + " Rcords processed in " + elapsedTime + " MS");
        } catch (Exception e) {
            out.println("<h1>ERROR</h1");
            e.printStackTrace();
        }
        out.close();
    }
}
