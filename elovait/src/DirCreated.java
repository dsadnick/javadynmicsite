import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by dsadnick on 7/22/15.
 */
@SuppressWarnings("ALL")
public class DirCreated {

    boolean isDirectoryBoolean = false;
    boolean dirBool = false;

    public boolean dirCreated(){

        try{
            File theDir = new File("images");
            theDir.mkdir();
            dirBool = true;

        } catch (Exception e){
            e.printStackTrace();
        }
        return dirBool;
    }

    public boolean isDirectory(String dir){

        if (Files.isDirectory(Paths.get(dir))) {
            isDirectoryBoolean = true;
        } else {
            isDirectoryBoolean = false;
        }
        return isDirectoryBoolean;
    }

}
