
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by dsadnick on 6/14/15.
 */
@WebServlet("/Login")
public class Login extends HttpServlet
{

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("Login.java ------>INIT() <---- Login.java");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HttpSession session = req.getSession();

        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        String ip       = req.getRemoteAddr();


        session.setAttribute("userName",userName);
        session.setAttribute("lastLogin", new Date());
        session.setAttribute("ip", ip);

        System.out.println("doPost <---- Login.java by: ----> " + userName);

        req.setAttribute("userName",userName);
        req.setAttribute("password",password);
        req.setAttribute("ip",ip);

        InfoBean infoBean = new InfoBean();
        infoBean.setIp(ip);
        infoBean.setUserName(userName);
        infoBean.setUserPassword(password);

        req.setAttribute("bean",infoBean);



        MySQLAccessInsert mySQLAccessInsert = new MySQLAccessInsert();
        mySQLAccessInsert.doGet(req, resp,  infoBean);

        /*Properties p = new Properties();
        p.put("user",userName);
        p.put("password",password);*/



        //req.getRequestDispatcher("docs/html/Landing.jsp").forward(req, resp);
    }
}
