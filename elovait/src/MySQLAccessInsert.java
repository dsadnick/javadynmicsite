
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.MidiDevice;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by dsadnick on 6/16/15.
 */
@WebServlet("/Insert")
public class MySQLAccessInsert extends HttpServlet{


    private static final String dbClassName = "com.mysql.jdbc.Driver";
    private static final String CONNECTION =
            "jdbc:mysql://127.0.0.1/mydb";
    java.sql.Connection connection = null;


        public void doGet(HttpServletRequest request, HttpServletResponse response,InfoBean infoBean) throws ServletException, IOException {

            ResultSet resultSet = null;
            String ip = request.getRemoteAddr();

            //InfoBean infoBean = (InfoBean) request.getParameter("bean");

            if (infoBean == null){
                try {
                    Class.forName(dbClassName).newInstance();

                    connection = DriverManager.getConnection(CONNECTION, "root", "root");
                    Statement statement = connection.createStatement();

                    statement.executeUpdate("Insert INTO users VALUES ('null','null','" + ip + "');");
                    //request.getRequestDispatcher("/HomePage").forward(request, response);
                    //connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else {
                try {
                    Class.forName(dbClassName).newInstance();
                    connection = DriverManager.getConnection(CONNECTION, "root", "root");
                    Statement statement = connection.createStatement();

                    statement.executeUpdate("Insert INTO users VALUES ('" + infoBean.getUserName() + "','" + infoBean.getUserPassword() + "','" + infoBean.getIp() + "');");
                } catch (Exception e) {
                    e.printStackTrace();
                }


                request.getRequestDispatcher("/HomePage").forward(request, response);
            }


        }

}
