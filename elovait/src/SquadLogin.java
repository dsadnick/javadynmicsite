import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/SquadLoginServlet")

/**
 * Created by dsadnick on 7/3/15.
 */
public class SquadLogin extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        SquadFound squadFound = new SquadFound();

        boolean userLogedIn = false;
        try{
            userLogedIn = squadFound.getSquad(userName,password);

            if (userLogedIn){
                System.out.println("user " + userName + " has loged in");
                request.setAttribute("userName",userName);
                request.setAttribute("logedIn", userLogedIn);
                Cookie cookie = new Cookie("userName", userName);
                response.addCookie(cookie);
                request.getRequestDispatcher("docs/html/SquadLanding.jsp").forward(request, response);
            } else {
                request.setAttribute("logedInError", true);
                request.getRequestDispatcher("docs/html/SquadLogin.jsp").forward(request, response);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
