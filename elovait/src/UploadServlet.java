import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dsadnick on 7/16/15. updated on 7/24/15
 */
@WebServlet("/UploadServlet")
@MultipartConfig(location="/tmp",
        fileSizeThreshold=1024*1024,
        maxFileSize=1024*1024*5,
        maxRequestSize=1024*1024*5*5)

public class UploadServlet extends HttpServlet {

    String image = "image";
    String userName = "";
    String userPkNumber;
    String comment;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        Cookie[] cookies = request.getCookies();


        String radio = request.getParameter("teamWin"); // Retrieves <input type="text"

        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        String fileName = getFileName(filePart);
        InputStream fileContent = filePart.getInputStream();//Actual Input Stream up file uploaded.

        comment = request.getParameter("comment");

        int imageNumber = new GetNumberOfImages().getNumber();//Returns number o fimages in /Images Folder



        for(Cookie cookie : cookies){
           if(cookie.getName().equals("userName")){
               userName = cookie.getValue();
           } else {
           }
        }

        String newFileName = imageNumber + fileName;

        if (new DirCreated().isDirectory(image)){
            //DIr allready exists just here for a back up.
        } else {
            new DirCreated().dirCreated();
        }

        ImageInfo imageInfo = new ImageInfo();

        try {
            String userPkNumber = new GetUserId().getId(userName);
            imageInfo.setUserForignPK(userPkNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Date createdDate= new Date();

        System.out.println(createdDate);

        //imageInfo.setUserForignPK(userPkNumber);
        //imageInfo.setDate(createdDate);


        if (radio == null){
            imageInfo.setTeamWin("n");
        } else {
            imageInfo.setTeamWin("y");
        }


        new ImageSaver().saveImage(fileContent,newFileName);


        imageInfo.setPicturePath(image + "/" + newFileName);//Left Off here need to get file path
        imageInfo.setComment(comment);

        PrintWriter out = response.getWriter();
        out.println(imageInfo.toString() + " test for tostring");

        try {
            new WriteImageToDataBase().write(imageInfo);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }






    private static String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

}
