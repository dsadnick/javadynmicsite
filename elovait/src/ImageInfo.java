import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dsadnick on 7/24/15.
 */
public class ImageInfo {


    String userForignPK;
    Date date;
    String teamWin;
    String picturePath;
    String comment;



    public String getUserForignPK() {
        return userForignPK;
    }

    public void setUserForignPK(String userForignPK) {
        this.userForignPK = userForignPK;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTeamWin() {
        return teamWin;
    }

    public void setTeamWin(String teamWin) {
        this.teamWin = teamWin;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String toString(){
        return "pk: " + getUserForignPK() + "\n date: " + getDate() + " \nTeamWin: " + getTeamWin() + " \nPicture Path: " + getPicturePath() + " \nComment: " + getComment();
    }



}
