/**
 * Created by dsadnick on 7/6/15.
 */
public class SquadList {



    private String userName;
    private String userFirstName;
    private String userLastName;
    private int userId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String toString(){

        String endString;

        endString = getUserName() + " " + getUserId() + " " + getUserFirstName() + " " + getUserLastName();

        return endString;
    }
}
